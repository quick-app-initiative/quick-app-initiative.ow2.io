---
title: Participants
subtitle: Organizations participating in the QAI
date: 2021-12-01
section: about
comments: false
breadcrumbs: false
---

QAI was launched during in June 2021 by the following organisations:

{{< static-gallery >}}
  {{< participant src="/img/participant-logos/alliancetech.png" link="./alliancetech/" alt="Alliance Tech" >}}
  {{< participant src="/img/participant-logos/ctic.png" link="./ctic/" alt="CTIC" >}}
  {{< participant src="/img/participant-logos/famobi.png" link="./famobi/"  alt="Famobi" >}}
  {{< participant src="/img/participant-logos/frvr.png" link="./frvr/" alt="FRVR" >}}
  {{< participant src="/img/participant-logos/huawei.png" link="./huawei/" alt="Huawei" >}}
  {{< participant src="/img/participant-logos/santillana.png" link="./santillana/" alt="Santillana" >}}     
{{< /static-gallery >}}

Recently, the following organizations also joined the QAI:

{{< static-gallery >}}
  {{< participant src="/img/participant-logos/e-foundation.png" link="https://e.foundation/" alt="/e/Foundation" >}}
  {{< participant src="/img/participant-logos/olisto.png" link="https://olisto.com" alt="Olisto" >}}
  {{< participant src="/img/participant-logos/startinblox.png" link="https://startinblox.com/en/" alt="Startin'Blox" >}}
{{< /static-gallery >}}

The Quick App Initiative is recognized and supported by: 

{{< static-gallery >}}
  {{< participant src="/img/participant-logos/systematic.png" link="https://systematic-paris-region.org/" alt="Systematic Paris Region" >}}
{{< /static-gallery >}}

Also you can check all the current [individual participants](https://www.ow2.org/view/QuickApp/Participants) of the initiative, in the OW2 website.  

## Get involved

The QAI is open to everyone:

* Any organization or individual regardless of geographic location;
* Operating system and device vendors that implement Quick App engines, marketplaces, and other supporting tools for Quick Apps;
* Content and service providers interested in end-user interactions using Quick Apps;
* Marketing experts interested in the promotion of Quick Apps as a paradigm;
* Developers, including professionals, hobbyists, and students, interested in Web and native app technologies;
* Public institutions, including municipalities, with specific needs such as accessible services for citizens and visitors;
* Research centers and academic institutions interested in innovation through agile technologies;
* Innovative entrepreneurs and SMEs.
* ...

So, [get involved now](/page/about/#see-you-soon)!