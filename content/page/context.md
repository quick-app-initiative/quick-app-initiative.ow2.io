---
title: Quick Apps
subtitle: And the broader MiniApp concept
section: About
date: 2021-11-29
comments: false
---

The **[OW2 Quick App Initiative](https://www.ow2.org/view/QuickApp/)** aims to foster a vibrant multi-party **Quick App** ecosystem in Europe and beyond.

## Quick Apps implement W3C MiniApp standards

The Quick App platform is a *concrete implementation* of [Standards](https://www.w3.org/TR/mini-app-white-paper/#what-is-miniapp) being drafted by leading technology companies within the [W3C MiniApp Working Group](https://www.w3.org/2021/miniapps/). MiniApp technology is based on a **front-end Web stack** (CSS, JavaScript, Web APIs ...) that leverages [MVVM design patterns](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) to provide **access to device features** and **powerful native rendering**.

> Quick Apps are a way to develop, package and distribute MiniApp compliant applications across platforms

Quick Apps facilitate the process of development through advanced UI components and predefined native APIs (e.g., push notifications, network monitoring, Bluetooth, camera ...). These enable developers to design and build usable, reliable and efficient applications *rapidly*.

- Compared to a native Android app, and looking at equivalent functions, Quick Apps require 20% less lines of code --> **easier for the developer**, lighter on download infrastructures, less impact on device storage.
- The process of updating and maintaining Quick Apps is more straightforward than for native apps --> developers can **easily update** their Quick Apps and efficiently deliver new versions to end-users.
- Quick Apps support multi-channel distribution using deep links, marketplaces, web-links, and specific device assistants --> app providers can **maximize app discoverability** and perform innovative marketing activities to promote their services and products.

## Why QAI?

The MiniApp concept and associated platforms, such as Quick Apps, are widely used by consumers and developers in Asia. In China, 12 leading phone vendors support Quick Apps *out-of-the-box*, including Huawei, Lenovo, OnePlus, Oppo, Vivo, Xiaomi, and ZTE. 

> 1.2 billion devices support Quick Apps, but the vast majority of these are in China and Asia.

Despite this massive popularity and the benefits highlighted above, outside of Asia the concept is largely unknown. Indeed, in Europe the Quick App ecosystem is still very much "greenfield". Developers, by and large, do not know the technology, businesses and consumers are unaware of the concept and its opportunities, and unlike in Asia, only Huawei devices come out-of-the-box with the necessary Quick App framework integrated with the OS.

**This is where the OW2 Quick App Initiative comes in:** lifting awareness, collaboratively creating a commons of knowledge, tooling and experiences, collecting needs to feed into W3C MiniApp standardisation efforts.
