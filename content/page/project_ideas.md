---
title: Ideas for Quick App projects
subtitle: POCs, Pilots and Business
section: Join In
date: 2022-01-31
comments: true
draft: false
---

Don't see a pending proposal that interests you? Interested in exploring the use of Quick Apps?

Take up the challenge, and lead one of the ideas below.<br>
Or, maybe these get your creative juices flowing and spark other ideas in you! :-)

## Visitors

### Places of interest
- **Distribute:** QR code by a park, a monument, an historic building.
- **User value:** Scan the code, get enriched info about the attraction in *your* language.
- **Business value:** Drive traffic towards participating merchants.

### Local guide
- **Distribute:** QR codes in high traffic areas of the town (stations, tourist information offices, hotels ...). QR codes/web links shared through marketing campaigns (print ads, social media marketing ...).
- **User value:** Scan the code, find out about the town, its places of interest, local events. Get discount vouchers. Buy travel tickets for your stay ...
- **Business value:** Drive traffic towards participating merchants.

### Conference
- **Distribute:**
- **User value:**
- **Business value:**

### Concerts
- **Distribute:**
- **User value:**
- **Business value:**


### Exhibition guide
- **Distribute:**
- **User value:**
- **Business value:**


## Transport


### Public transport
- **Distribute:**
- **User value:**
- **Business value:**

### eMobility
- **Distribute:**
- **User value:**
- **Business value:**


## Marketing


### Collectibles
- **Distribute:**
- **User value:**
- **Business value:**


## Small business


### Short term vacation rental
- **Distribute:**
- **User value:**
- **Business value:**


### Restaurant
- **Distribute:**
- **User value:**
- **Business value:**


## Consumer


### Shopping center signage & promotions of the day
- **Distribute:**
- **User value:**
- **Business value:**

### Food facts
- **Distribute:**
- **User value:**
- **Business value:**


