---
title: About us
subtitle: About the Quick App Initiative
section: About
date: 2021-11-29
comments: false
---

The **[OW2 Quick App Initiative](https://www.ow2.org/view/QuickApp/)** explores technologies, use cases and business dynamics centered on *Quick Apps*, a new paradigm of light, no-install, hybrid applications for smart devices.

> - What are Quick Apps & why create an initiative? [Find out here](/page/context).
> - Who are the particpants? [Find out here](/page/participants/).

The initiative is a respectful, consensus driven community, founded on the ideals of *open source* (transparency, collaboration, sharing, open to all). It operates in accordance with a community agreed [charter](https://miniapp-initiative.ow2.io/docs/charter.pdf), and is overseen by [OW2](https://www.ow2.org/view/Main/), one of Europe's preeminent open source organisations.

In August 2021, the initiative proudly gained [peer recognition](https://systematic-paris-region.org/le-hub-open-source-apporte-son-soutien-au-projet-de-lassociation-europeenne-ow2-sur-les-quick-apps/) from [Systematic Open Source Hub](https://systematic-paris-region.org/hubs-stakes/open-source-hub/?lang=en); a deep-tech innovation pole for the Paris region.

- **Learn** about quick apps in the [White Paper](https://miniapp-initiative.ow2.io/page/whitepaper)
- **Subscribe** to the [mailing list](https://mail.ow2.org/wws/subscribe/quickapp?previous_action=info)
- **Get stuck in** with the [start guide](https://miniapp-initiative.ow2.io/developers/guide/getting-started.html)
- **[Ask the team a question](mailto:quickapp-team@ow2.org?subject=I%20have%20a%20question%20about%20QAI)**

## Why Quick Apps?

The mobile app dynamic, born (so to speak) in 2008, has been a revolution in how we use our phones, how we access with the internet, how we interact with our family, friends, and colleagues. It spawned innumerable new business opportunities and paved the way for amazing new services. Yet, it's difficult to imagine how the *current* app dynamic can efficiently, sustainably and inclusively scale with the growing needs of smart homes, smart cities, connected transport, tele-health, and the general societal shift towards online everything.

We strongly believe that Quick Apps [scratch an itch](/page/scratch-an-itch) ... indeed several itches.

Not only do they scratch itches, which is already great, they also enable new services and products which would be clumsy to achieve using the current mobile app model. In a way, they merge what is great about the web, with what is great about mobile; in-the-moment actions, no install, background notifications, access to device hardware capabilities, offline caching, monetization routes, reduced development, reduced network usage, reduce device storage bloat ...

You can read more thoughts about Quick Apps in the [editorials section](/editorials/).

## Initiative aims

The initiative aims to encourage, support and foster a _vibrant and diverse_ quick app ecosystem in Europe (and beyond) to deliver on the promise of quick apps.<br>It has 4 ambitions:
1. Raise quick app awareness within developer, business and consumer communities;
2. Build and share a commons of quick app knowledge and experience;
3. Build and share quick app tooling (for example, tools, code templates, plugins for IDEs, test-beds, etc.);
4. Support W3C MiniApp Working Group activities.

The initiative brings together a **multidisciplinary group of people** from **different organizations and countries**. It covers a wide range of industries and topics, fosters innovation and entrepreneurship, advocates for core values such as sustainability, resilience, user privacy and ethical use of the technology.

-	**An open community**: Any organization (public, private, academia, research ...), or individual, may participate in the activities and become a participant of the initiative;
-	**Vendor-neutral**: The initiative is focused on raising quick app concept awareness, developing tools and documentation, as well as exploring use cases from a vendor-neutral perspective;
- **Transparent and driven by group’s consensus**: Resolutions of the initiative and its activities are based on community consensus under the principles of openness and transparency;
- **Topic-oriented work**: Participants may propose specific *Task Forces* to better enable focused collaboration around a specific need or objective;
- **Open Source advocate**: The initiative is committed to the open source paradigm, fostering the production and release of Free Libre and Open Source code (OSI or FSF approved license), open documentation and open data (Creative Commons).

## Interested?

Everyone is welcome to participate to the initiative; [find out how here](/get-involved/).<br>
Including, sponsoring and/or participating to [project proposals](/get-involved/project-proposals/).

There are **no fees** to participate to the initiative.<br>
There are **no obligations** to join OW2 (although they always appreciate [having people join them](https://www.ow2.org/view/Membership_Joining/On_Line_Registration)).

## See you soon!

- To **join QAI**, register here: https://www.ow2.org/view/QuickApp/Participants_Form
- To **follow initiative news**, sign up to the mailing list: https://mail.ow2.org/wws/info/quickapp
- To **ask the team a question**, email us here: [quickapp-team@ow2.org](mailto:quickapp-team@ow2.org?subject=I%20have%20a%20question%20about%20QAI)

Collaboration tools:
- GitLab space: https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative
- Rocket.Chat discussion space: https://rocketchat.ow2.org/group/QAI-Town-Square

Resource spaces:
- General information: https://miniapp-initiative.ow2.io/
- Developer documentation: https://miniapp-initiative.ow2.io/developers/guide/

Social media:
- Twitter: [@OW2QuickApps](https://twitter.com/OW2QuickApps)
- Hashtag (for Twitter and LinkedIn): #QuickAppsEU
