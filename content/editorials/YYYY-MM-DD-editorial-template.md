---
type: post
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# Tags are comma seperated
# highlight=true if you want this item to be listed on the homepage 
# draft=true if you do not want this shown on pages or in navigation (attention: the page is still live, it is just not linked)
excerpt_separator: <!--more--> 
title: My first editorial
subtitle: this is my editorial
author: FirstName Surname
date: 2022-10-30
tags: ["Tag 1","tag 2"]
bigimg: [{src: "/img/mockups.jpg", desc: "MiniApps"}]
highlight: true
comments: true
draft: true
---


[Summary before the "more" tag]

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus iaculis elit orci, eget tempus arcu ullamcorper ut. Ut ut finibus ante, vehicula aliquam dolor. Donec viverra nisl ut elit convallis, quis molestie ligula dapibus. Curabitur nec luctus ligula. Aenean blandit nulla et purus luctus condimentum. Maecenas quis nulla eget nulla sollicitudin rutrum. Sed at ligula non eros sollicitudin semper id vel nisi. Nullam risus diam, ultricies sodales malesuada ac,

[end of excerpt]

<!--more-->

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus iaculis elit orci, eget tempus arcu ullamcorper ut. Ut ut finibus ante, vehicula aliquam dolor. Donec viverra nisl ut elit convallis, quis molestie ligula dapibus. Curabitur nec luctus ligula. Aenean blandit nulla et purus luctus condimentum. Maecenas quis nulla eget nulla sollicitudin rutrum. Sed at ligula non eros sollicitudin semper id vel nisi. Nullam risus diam, ultricies sodales malesuada ac, rhoncus quis leo. Sed nisl massa, pulvinar vitae venenatis a, dictum id lectus. Maecenas aliquam sit amet mi eget faucibus. Fusce maximus nulla sed posuere lobortis. Cras luctus diam eu erat blandit, vitae maximus mi viverra.


## You can use headings (H2+)

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis lacinia eu metus at pulvinar. Nam vel sapien sed ante interdum consectetur nec at velit. Nam rutrum tellus id efficitur pretium. Aenean velit nisl, maximus at leo non, porta fringilla dui. Pellentesque congue orci a massa lobortis ultrices. Maecenas vitae sollicitudin tortor. Etiam mauris ligula, ornare id facilisis et, blandit eu justo. Ut faucibus, dui et lobortis dapibus, ex diam finibus dolor, et vestibulum sapien leo nec tortor. Fusce porttitor justo id erat ullamcorper luctus.

Pellentesque porttitor luctus scelerisque. Quisque sollicitudin nisl vitae nisl placerat, quis mollis sapien ullamcorper. Vivamus at massa nulla. Etiam rhoncus fermentum sollicitudin. Nunc ut dui nec ante sagittis mollis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer ornare enim erat. Morbi mattis ultricies magna vitae vestibulum. Proin tempor, erat quis scelerisque bibendum, libero dolor suscipit sem, non lacinia orci risus vitae metus. Curabitur congue, nisl eget volutpat tempus, arcu augue maximus nisi, eget placerat ipsum velit lacinia nisi. Curabitur vitae luctus magna. Etiam eget massa vel nulla dictum pharetra.

Integer vel vulputate dolor. Aenean aliquam posuere elit, id porta ligula ullamcorper at. Ut sit amet luctus diam. Vivamus nec nulla elementum nunc tempor luctus id mollis velit. Curabitur sit amet fringilla ipsum. Nam molestie tempus varius. Ut aliquam dolor odio, et blandit diam vehicula ac. Integer molestie neque massa, sed blandit nisi laoreet vitae. Cras ut convallis sapien.

Aliquam sodales vestibulum nibh, id facilisis velit dapibus nec. Mauris dignissim sit amet mauris id euismod. Suspendisse sed enim vel neque convallis accumsan. Nunc venenatis at nulla in faucibus. Curabitur id elementum lorem, non tempor sapien. Nulla malesuada, ligula vitae feugiat volutpat, neque mi eleifend nunc, sed tristique tortor elit et tortor. Nam elementum nunc vel turpis suscipit egestas. Vivamus vehicula tortor pretium semper aliquet. Integer tincidunt est tellus, vel convallis neque viverra eget. Nunc in ante vitae nisi facilisis ullamcorper. Aenean placerat nec nunc non semper. Duis felis velit, ornare eget felis non, porta consequat turpis. Vestibulum ultricies aliquam convallis.
