---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
# 
# THIS ARTICLE IS MOVED TO "NEWS" ... THIS FILE REMAINS TO AVOID BROKEN LINKS
# 
highlight: true
draft: true
title: Your town deserves a quick app
subtitle: Develop your own quick app in minutes
author: Martin Alvarez
date: 2022-06-08
tags: ["code","pilot"]
bigimg: [{src: "/img/mockups.jpg", desc: "MiniApps"}]
comments: true
---

Every single town has its singularities. Some might be famous for their cultural heritage, others for gastronomy, winemaking, or [unique museums](https://planomagazine.com/planos-past-the-cockroach-hall-of-fame-museum/). All towns have a fascinating history behind them, and their inhabitants have knowledge that complements the official archives. Large cities usually have tourism resources to publish online and offline guides highlighting their assets, but this is unattainable in smaller towns with fewer resources. Having this global challenge in mind, we have created a low-code solution for anyone interested in revealing their hometown's famous and unknown gems. You only need a list of points of interest (PoI).


<!--more-->

> It's free, open-source, based on open data, and collaborative.

![Screenshot of the quick app generated with this project](https://pbest.eu/comic-book-route-quick-app/bxl/images/screenshots.gif "Brussels Comic Book Route Quick App")

The [Local Cultural Heritage Quick App](https://github.com/pbesteu/poi-quick-app) is an open-source project that promotes cultural heritage collections or other interesting PoIs, in small and medium cities through open data and crowdsourcing. 

As [presented at the OW2Con22](https://www.ow2con.org/view/2022/Abstract_Community_Day#08061649), you can start a new project using existing open datasets -if you have an open data initiative around, go there and ask for the data- and automatic processing of the data through [simple data curation tools](https://openrefine.org/). The app's database will be stored in a public git repository so local experts might refine the app's content, including descriptions, names, and pictures, adding new points of interest to the app without touching anything of the code. At the same time, developers can enhance the initial code of the app, adding new features aligned to the scope and theme of the app.  

As you can see in the repository, the project is structured in two main parts: 
- the **quick app** code you can customize, pack and publish; and 
- the **database**, a JSON document with the PoIs.

 
### The database

The content of the application is stored in a particular directory in the git repository to be published using _Github pages_, _Gitlab pages_, or similar service (content must be served online).   

Every project has two basic resource types:
- images (`./images/xxxxx.jpg`): light pictures with a homogenous look and feel; the lighter, the better.
- database (`./data.json`): JSON file with the app's configuration (name, colors, privacy texts, etc.) and the PoIs you want to show in the app.

External users can download it locally, modify the texts, or add a new element based on their knowledge. They also can upload the changes directly (better a pull request if they are familiar with git) or raise issues to suggest the changes. You can establish your update and moderation mechanisms based on your preferences.

In `./sample/data.json` you have an empty file you can use to start the project's database. Please ensure that this document has the correct format (syntax and content). The _Github_ CI workflow includes a document validation against the JSON schema you can find in the repository (`schema.json`) to guarantee the format of the database.

This JSON document contains two main parts, represented by the keys of the root object:

- `meta`: metadata about the project
- `content`: multilingual content and setup of the application (based on localized information)

#### Project's metadata 

Example of a project for the City of Paris:

```json
{
    "meta": {
        "app_title": "Paris Street Heritage",
        "version": 1,
        "updated": "2022-03-26",
        "source_url": "https://pbesteu.github.io/cultural-heritage-quick-app/fr/paris/data.json"
    },
    "content": {
        "en": {},
        "fr": {},
        "es": {}
    }    
}
```

#### Content

Continuing with the example of City of Paris, we can localize in any language using the [ISO 639-1](https://www.loc.gov/standards/iso639-2/php/English_list.php) code (assigning a two-letter code for the language). You can also specify the concrete region (using a `-` character and the concrete [ISO 3166-1](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes) (Alpha-2 code).

```json
{
    "meta": {},
    "content": {
        "en_GB": {},
        "en_US": {},
        "fr": {},
        "es": {}
    }
}
```

The project must contain at least one language tag.

Every content object key is a language tag that must contain JSON objects with the same keys but with different translations and content localized according to the language.

The content language tags are objects with the following structure:

```json
{
    "meta": {},
    "content": {
        "LANG": {
            "app": {},      
            "pois": []      
        }
    }
}
```

Where:
- `app` is an object that contains general information to configure the application for this language.
- `pois` is an array of objects with the points of interest or items of the database.

```json
{
    "meta": {},
    "content": {
        "en": {
            "app": {
                "theme": {
                    "brand": "#B11623",
                    "complementary": "#FAFAFA"
                },
                "repository_url": "https://example.github.io/cultural-heritage-quick-app/tree/main/docs/fr/paris",
                "text_info": "This project was created by...",
                "text_acknowledge": "We would like to thank...",
                "text_feedback": "Please let us know if you want to contribute...",
                "feedback_url": "https://example.github.io/cultural-heritage-quick-app/fr/paris/#contributors",
                "issue_url": "https://github.com/example/cultural-heritage-quick-app/issues/new?labels=fr/paris"
            },
            "pois": [
                {
                    "id": "eiffeltower",
                    "lat": "48.865157817905896",
                    "lon": "2.2938185078918156",
                    "type": "tower",
                    "name": "Eiffel Tower",
                    "images": [
                        "https://example.github.io/cultural-heritage-quick-app/fr/paris/images/eiffeltower_1.jpg",
                        "https://example.github.io/cultural-heritage-quick-app/fr/paris/images/eiffeltower_2.jpg"
                    ],
                    "description": "The Tour Eiffel (French) is an iron lattice tower, named after the engineer Gustave Eiffel, whose company designed and built the tower",
                    "more": "Locally nicknamed La dame de fer (French for Iron Lady), it was constructed from 1887 to 1889 as the centerpiece of the 1889 World's Fair and was initially criticized by some of France's leading artists and intellectuals for its design, but it has become a global cultural icon of France and one of the most recognizable structures in the world",
                    "attributions": [
                        "John Smith", 
                        "Jane Doe"
                    ], 
                    "urls": [
                        "https://en.wikipedia.org/wiki/Eiffel_Tower"
                    ]
                }
            ]
        }
    }
}
```

##### App configuration

Depending on the user's locale, the app may show localized texts, URLs, and themes. Each language tag in the content member has an object to configure and customize the application (app key). You can customize it through the following members:

- `theme`. The main look-and-feel colors:
  - `brand`. The primary color in HEX format (e.g., `#B11623`)
  - `complementary`. The secondary color in HEX format (e.g., `#FAFAFA`)
- `repository_url`. The URL to the repository where the project is hosted (where this database is maintained).
- `text_info`. Text to explain the project ('about' section).
- `text_acknowledge`. Text with acknowledgments about the project ('about' section).
- `text_feedback`. Text with information explaining how to contribute to the project ('about' section).
- `feedback_url`. A URL linking to a page where the contributors can get involved ('about' section).
- `issue_url`. A URL with customized parameters in the query string that will be used to report the individual issues on specific items. It will be linked to the individual items. 

##### Points of Interest

The PoIs are the main items of the database, described in the poi attribute as an array of objects with the following members:

- `id`. Unique identifier for the item (e.g., `eiffeltower`).
- `name`. Short text with the title of the point of interest (e.g., Eiffel Tower).
- `lat`. Latitude component of the item coordinates in WGS84 format (e.g. 48.8651).
- `lon`. Longitude component of the item coordinates in WGS84 format (e.g., 2.2909).
- `type`. Short text with the type of the item, according to your own taxonomy (e.g., sculpture, painting,...).
- `images`. One or more absolute URLs with pictures about the item. These images could be external, but it is recommended to host them under the same repository as the database. See How to submit new images for more details.
- `description`. One paragraph text with a concise description of the item.
- `more`. Extended text with more details of the item.
- `urls`. One or more absolute URLs to external related resources (e.g., Wikipedia links, official sources, etc.).
- `attributions`. List of texts with the names of the contributors to the content or images of this item, if any.

### The Quick App

You will find the code of the quick app in the /quick-app folder of the repository. You can modify the code and adapt it to your concrete implementation, but the critical tasks to do are the following.

#### Change logos and images
In `/quick-app/src/common/images/`, you will find the images used in the app:  the main logo of the app (`logo.png`) and the icon used for the home-screen installation (`logo-216.png`). 

#### Load the first version of the database
In `/quick-app/src/common/json/data.json`, you should load the first version of the database with the configuration and PoIs. This local configuration is only used if the quick app is running offline, but it will be superseded by the external database in case the remote version is more actual.    

#### Update the manifest.json

Modify the first lines of the manifest with the app's identifier (`package`) and the app's official name. Learn more [about the quick app manifest](https://miniapp-initiative.ow2.io/developers/guide/manifest.html) if you want to change other metadata.

``` json
{
    "package": "org.example.paris",
    "name": "Paris Cultural Heritage",
    "versionName": "0.0.1",
    "versionCode": 1,

}
```

See all the code of the example shown at the OW2Con22 about the [Brussels Comic Book Route](https://github.com/pbesteu/comic-book-route-quick-app).

Just clone or fork the [main repository](https://github.com/pbesteu/poi-quick-app) to launch your project now. Start by modifying the templates and run your quick app in minutes. 

And don't forget to [let us know what you have done](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/issues/new) so far with these templates.
