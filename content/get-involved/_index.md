---
title: Ready to join in?
subtitle: (the more the merrier)
section: Join In
date: 2022-01-27
comments: false
draft: false
---

The MiniApp Initiative fosters the collaborative authoring of **documents**, developer **tools** and runtime **software engines** under [OSI](https://opensource.org/licenses) or [Creative Commons](https://creativecommons.org/about/cclicenses/) recognized licenses.

In general, projects and deliverables are hosted within the [initiative's OW2 GitLab repository](https://gitlab.ow2.org/miniapp-initiative/miniapp-initiative). You can also visit the developer section of this portal to see some of the [projects](/page/developers/code) and [documents](/page/developers/docs) already created.
