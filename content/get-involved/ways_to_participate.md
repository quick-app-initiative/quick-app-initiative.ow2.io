---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: Many ways to participate
subtitle: 
section: Join In
date: 2022-01-27
comments: false
draft: false
---

There are many ways to participate to the initiative, but of course the most important criteria is your interest to discover the amazing potential this dynamic offers, and your motivation to help foster a nascent ecosystem within Europe and beyond.

<!--more--> 

We strive to create an eclectic melting pot of people within the initiative, no matter:
- the skillset (business strategy, web standards, marketing & communication, web and mobile app development, academia ...).
- the domain of activity (gaming, sports & health, finance, highstreet commerce, transport & mobility ...).
- the experience level (from students, to home hobbyists, to CEOs).
- the company size (from startups, to SMEs, to internationals).

You determine your participation in relation to your motivations and time availability.

**Task Forces**
- Task Forces are created to coordinate _multi-party_ advancement of a given topic. They result in _actionable deliverables_ such as code projects, whitepapers, guides and events.
- Each _active_ Task Force has a lead coordinator.
- Initiative participants have so far created 3 Tasks Forces:
  - **Gaming** (led by FRVR),
  - **Sustainability** (led by Alliance Tech)
  - **Education**.

**Use cases**
- Maybe you would like to help us deepen and explore use cases across different domains (tourism, health, education, sport, commerce, ...).
- Use case exploration is materialised within initiative Task Forces as opportunity studies, proof of concepts, pilots, hackathons, events, etc.

**Community animation**
- Community is where the heart is, and open source is all about community.
- We would love to hear from you if you are interested in being an *ambassador* for the initiative.
- Help us organise or participate to:
  - meet-ups, hackathons and events in your town and in your local language.
  - webinars and talks for people and organisations in your domain of activity.
- Help us gather feedback from your local community about how MiniApps could be used, or evolved, to address needs.
- Help us animate our social media presence.
- Write a [blog](/editorials/) article.
- Spread the word and attract new participants :-)

**Technologies**
- Do you have experience in mobile app development, web technologies (JavaScript, CSS, HTML5 ...), browser engines, webview or PWA technologies?
- Could you help deepen, expand or translate online [developer documentation](https://miniapp-initiative.ow2.io/developers/guide/)?
- Would you be interested in collaborating on developer tools, code templates & samples, or even an open source MiniApp engine?

**Development of pilot apps**
- Let us know if you would like to help develop MiniApp POCs and pilots; your help is gratefully received.

