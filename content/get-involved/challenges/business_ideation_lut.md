---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: Accelerating Innovation through structured openness
subtitle: Industry-academia challenges with LUT University
author: Nikhil Phadnis
date: 2023-07-07
tags: ["industry-academia","challenges","Telanto","LUT"]
bigimg: [{src: "/img/challenges/lut-university-2.jpg", desc: "Photo from LUT University Press Room"}]
section: Join In
highlight: true
draft: false
comments: true
---

Companies always perceived university-business collaboration as a secondary approach to accelerate innovation until the 2000s. Only lately has it become quite popular among companies actively seeking out solutions.

<!--more-->

In February 2023, Huawei participated in a university-business collaboration through Telanto, a university-business collaboration matchmaking platform with Lappeenranta Lahti University of Technology ([LUT University](https://www.lut.fi/en)) in Finland.

> LUT University is ranked in the top 350 universities by Times Higher Education and the top 11 world's best small universities. 

The collaboration took place from February to May 2023 for twelve weeks. Twenty Students from LUT University's *Open and Collaborative Innovation course (2023)* participated to this collaboration. Huawei provided challenges to students, where students were free to creatively approach the challenge as they saw fit to address the project goals. Huawei presented a pitch to students and asked them to, “identify areas of application/innovative use cases for Quick App technology”.

Students were divided into multidisciplinary groups of five, with a total of four teams solving the challenge. Additionally, LUT University follows a mentor system, meaning each team is assigned a mentor to guide, facilitate and direct the students with active input that ensures project efficiency and complete deliverables. Moreover, Christian Paterson -an open source expert assisting Huawei- provided active feedback to the students in each project stage.

The students followed the stage gate process, with three stages showcasing their work, gathering active feedback and improving their outputs based on the requirements of Huawei. At the end of April 2023, Huawei received four high-quality final presentations and reports with unique, innovative use cases and their potential feasibility for commercial scale-up. The collaboration resulted in promising results for the industry to adapt and explore their commercial potential to aid the adoption of Quick-apps globally.

## Challenge description

The following is extracted from Huawei's business ideation challenge:

> Explore how the dynamic of MiniApps/Quick Apps (or other light application technologies) might impact the articulation and collaboration points between application developers, publishers and the wider marketplace as a way of better serving consumers, increasing business value for all stakeholders along the value chain, and leveraging different distribution and communication channels and features.
> 
> Each team can select any of the following goals to answer:
> 1. Identify a vertical where you believe Quick Apps could *change the game*.
> 2. Identify Quick Apps opportunities for health and/or fitness and/or aging populations. Could Quick Apps form part of an integrated approach that leverages smart devices, health trackers, bio monitors etc.?
> 3. Determine if and how Quick Apps might help reduce ecologic impacts or help address sustainability concerns in the fields of tourism or fashion.
> 4. Identify opportunities for Quick Apps in emerging markets.
> 
> Goals should be illustrated with detailed use case ideas. Ideally, exploring end-to-end customer journeys and/or value chains.
> - Where possible determine appropriate business models and estimate the business potential for your idea(s). 
> - Why should your use-case(s) be taken from concept to prototype?
> - What hurdles might your idea face?
> - Do you think that open source can play a role in achieving any of the following for your use case: market reach/growth/capture, prototyping, skills acquisition, cost (CAPEX/OPEX) optimisations?

## Final deliverables from LUT University students
- Team 1
  - Developed a comprehensive MiniApp/Quick App business case for **hyperlocal community services and activities**
  - [Presentation](/docs/challenges/2023_h1/lut_team1_final_presentation.pdf)
  - [Report](/docs/challenges/2023_h1/lut_team1_final_report.pdf)
- Team 2
  - Explored the  potential for using MiniApps/Quick Apps to help patients follow **out-of-clinic physiotherapy care programs**
  - [Presentation](/docs/challenges/2023_h1/lut_team2_final_presentation.pdf)
  - [Report](/docs/challenges/2023_h1/lut_team2_final_report.pdf)
- Team 3
  - Examined the many opportunities MiniApps/Quick Apps could provide in **airport traveller experiences**
  - [Presentation](/docs/challenges/2023_h1/lut_team3_final_presentation.pdf)
  - [Report](/docs/challenges/2023_h1/lut_team3_final_report.pdf)
- Team 4
  - Delved into the idea of using MiniApps/Quick Apps for **street vendor cashless payments in Thailand and Asia**
  - [Presentation](/docs/challenges/2023_h1/lut_team4_final_presentation.pdf)
  - [Report](/docs/challenges/2023_h1/lut_team4_final_report.pdf)
