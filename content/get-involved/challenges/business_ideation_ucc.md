---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: Exploring the Future of Mobile Applications
subtitle: The Quick Apps Challenge by UCC Students
author: Dr. Catherine Kavanagh and Mr. Tomas Kenneally, Module Lecturers  
date: 2023-07-11
tags: ["industry-academia","challenges","Telanto","UCC"]
bigimg: [{src: "/img/challenges/University-College-Cork-Panorama-2012.jfif", desc: "Von Bjørn Christian Tørrissen, CC BY-SA 3.0"}]
section: Join In
highlight: true
draft: false
comments: true
---

The rapid evolution of the digital era has brought about significant changes in the way we interact with the world around us. Amid this transformation, post-graduate master's level students at [University College Cork, Ireland](https://www.ucc.ie/en/), embarked on a business challenge centred around Quick Apps. The students had the opportunity to delve into the evolving landscape of mobile applications and analyse the impact of Quick Apps and other light application technologies on the mobile ecosystem.

<!--more-->

Quick Apps, as representatives of light application technologies, provide an intriguing gateway to reimagine our interaction with the digital world. **Quick Apps represent a paradigm shift in the mobile application landscape**. The students delved into the specifics of Quick Apps during their research and discovered the manifold benefits they offer. These include low-cost development, no-installation requirements, native experience, easy discoverability, and high user retention rates.

>**With their foundation on emerging W3C MiniApp standards, Quick Apps effectively merge the power of traditional mobile applications with the immediacy of web-based experiences**.

The challenge presented to the students encompassed various dimensions, urging them to examine the current problems and difficulties associated with the prevailing mobile application landscape. Additionally, they were encouraged to identify the opportunities Quick Apps and other light application technologies can bring to consumers and businesses, while considering the challenges and risks associated with adopting this new dynamic. The business challenge presented to the students entailed a comprehensive examination of the current mobile application landscape. They investigated the issues and difficulties inherent in the status quo, while also exploring the efforts made by some industry players to challenge and overcome these limitations. 

To tackle the challenge comprehensively, the students adopted a holistic approach, considering perspectives from the business, consumer/user, technical, and developer standpoints. Each team focused on specific goals, exploring detailed use case ideas that illustrated end-to-end customer journeys and value chains. Their analysis aimed to go beyond surface-level research, emphasising innovative thinking and exploring potential business models, regulatory frameworks, and hurdles that their ideas might face. This multidimensional analysis enhanced the students critical thinking, problem-solving, and research skills. This challenge fostered innovative thinking and helped the students refine their abilities to ideate, design, and propose solutions that could shape the future of mobile applications.

The students also explored the role of open source technologies in achieving their use case objectives. They considered how open source solutions could impact market reach, prototyping, skills acquisition, and cost optimizations. Furthermore, they evaluated the positioning of open source within the research-to-anticipation-to-market pipeline and its potential impact on their business models and strategies.

> **Given the vast potential of Quick Apps, the students identified specific regions where these applications could have a significant impact**.

They prioritised countries based on market potential, adoption readiness, and other relevant factors. Moreover, they recognized the importance of key players and sought ways to engage them, considering their expertise, resources, and potential contributions to the Quick Apps ecosystem.

Throughout the challenge, the students benefited greatly from the guidance and mentorship provided by [Christian Paterson](https://www.linkedin.com/in/cpaterson2015/). Christian’s expertise and industry insights served as an invaluable resource for the students as they navigated the complexities of the mobile application landscape. His mentorship not only provided practical advice but also encouraged the students to think critically, challenge assumptions, and explore novel approaches. Christian’s presence as a mentor further enriched the students' learning experience, enabling them to bridge the gap between academic knowledge and real-world application.

> **Ultimately, this challenge enhanced the students learning and knowledge, preparing them to navigate the ever-evolving digital landscape and contribute to shaping the future of mobile applications.**

In conclusion, the business challenge undertaken by the students has been a transformative experience.

The Quick Apps challenge went beyond traditional academic coursework, immersing the students in a practical, hands-on experience that sharpened their skills and expanded their knowledge. Through their research and analysis, they gained insights into the disruptive potential and opportunities these technologies offer across various verticals and emerging markets. They also recognised the challenges and risks associated with adopting this new dynamic. 

## Final deliverables from UCC University students
- Team 1
  - **Identifying Health and Fitness Opportunities for the Quick App Industry.**
  - [Presentation](/docs/challenges/2023_h1/ucc_team1_final_presentation.pdf)
  - [Report](/docs/challenges/2023_h1/ucc_team1_final_report.pdf)
- Team 2
  - **How Quick Apps Can Address Ecological Concerns in the Fashion Industry**
  - [Presentation](/docs/challenges/2023_h1/ucc_team2_final_presentation.pdf)
  - [Report](/docs/challenges/2023_h1/ucc_team2_final_report.pdf)
- Team 3
  - **Where can Quick Apps change the game (for example, shopping or catering industries)**
  - [Presentation](/docs/challenges/2023_h1/ucc_team3_final_presentation.pdf)
  - [Report](/docs/challenges/2023_h1/ucc_team3_final_report.pdf)
- Team 4
  - **Quick App's Market Opportunity Analysis Report for Latin America - Brazil Case**
  - [Presentation](/docs/challenges/2023_h1/ucc_team4_final_presentation.pdf)
  - [Report](/docs/challenges/2023_h1/ucc_team4_final_report.pdf)
