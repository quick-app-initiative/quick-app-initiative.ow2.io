---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: Academia challenges
subtitle: What form do they take?
author: Christian Paterson
section: Join In
date: 2023-05-15
comments: false
draft: false
---

Industry-academia challenges can take several forms, and generally speaking, run for about 3 months:
- Studies
- Code challenges
- Hackathons
- Virtual internships

<!--more-->

## Large audience

Challenges are drafted on the Telanto platform, and once published made visible to the huge number of establishments within the Telanto academic network, or if preferred by the challenge sponsors/organisers, a selected sub-set of the establishments. Indeed, challenges can be targeted at different levels of education (undergraduate, masters, PhD, ...), different regions or countries (Europe, South America, France, Italy ...), and different course types (technical, marketing ...).

Interestingly, challenges can be made open for several students teams, or even several establishments, to take up.
(A student team consists of approximately 5 students and their supervising professor.)

## Independent

Challenges are never imposed on students, nor are students, professors, or education establishments paid to take part in challenges or even be part of the Telanto academic network.

It is the university professors and their students that decide if a challenge is interesting enough to answer.
- Students are motivated because challenges form part of their evaluated course work.
- Professors are motivated because challenges bring real world industrial exposure to their students.
- Universities are motivated because working with industry is a key attractiveness factor for their establishment.

Since the uptake of challenges is at the discretion of the students and their professors, there is no guarantee that a challenge will find participants. In this case, the challenge sponsors/organisers might want to rethink the challenge to increase its attractiveness.

## Public deliverables

As possible, challenge outputs are published via the OW2 Quick App Initiative and/or public forge. For such sharing to occur, written deliverables must be provided under a [Creative Commons By 4.0](https://creativecommons.org/licenses/by/4.0/) license, and software provided under an [OSI open source license](https://opensource.org/licenses).

>**_Nota bene_**
>
>Huawei has opened a contract with Telanto in order to organise challenges and wishes to share the possibility to create MiniApp/Quick App related challenges on the Telanto platform with QAI registered participants under the following conditions:
>- Huawei remains the relationship owner with Telanto and, in its sole discretion, has a final say on which challenges can be submitted to the platform and which QAI participants may be granted/rescinded platform access.
>- Huawei retains oversight of all challenges.
>- All patents, copyrights, trademarks and challenge deliverables remain with their respective owners.
>- Telanto requires that platform users must not contact establishments or students beyond the scope of any particular challenge.
>- Huawei will not accept any liability for the abuse and/or misuse of the Telanto platform by the respective QAI registered participants.
>- All written content should be delivered under a [Creative Commons By 4.0](https://creativecommons.org/licenses/by/4.0/) license.
>- All software code content should be delivered under an approved [OSI open source license](https://opensource.org/licenses). The [Apache License, Version 2.0](https://opensource.org/licenses/Apache-2.0) is preferred.
