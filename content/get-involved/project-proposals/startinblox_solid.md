---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: Startin'blox & Solid
subtitle: Collaborative project proposal
section: Join In
date: 2022-01-27
comments: false
draft: false
---

Startin'blox produce an open platform that allows app developers to incorporate pre-built *modules* of rich functionality in their apps. Modules leverage the W3C Solid framework created by Sir Tim Berners-Lee to promote data storage decentralisation, and the return of data control from service providers to data owners. The proposal is to bring Startin'blox and Quick Apps closer together.

<!--more-->

### Participants
- Lead: Startin'blox (France)
- Additional: none

### Aims
- A 6 month **project**.
- Enable developers who use the Startin'blox platform to output their applications as Quick Apps as well as PWAs.
- Enable Quick App developers to use the growing library of Startin'blox modules to enrich their Quick Apps with rich functionality.

### Requested assistance
- Sponsorship funding: 95K€

### Status
- Project accepted by the Steering Committee.
- Sponsors being sought.

### Read more
- [Project proposal slides](/project-proposals/Startinblox_proposal_call4assistance.pdf).
- [W3C Solid](https://www.w3.org/community/solid/).
