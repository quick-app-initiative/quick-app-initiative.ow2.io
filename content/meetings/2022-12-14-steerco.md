---
type: post
title: Steering Committee no.7
subtitle: December 14th 2022
author: Christian Paterson
date: 2022-12-22
highlight: true               # true or false -> true will be shown on the homepage 
tags: ["steerco"]
bigimg: [{src: "/img/1308819_round-table.jpg", desc: "Round-table discussion"}]
draft: false
---

Please find below links to the support slides and minutes from our 7th QAI Steering Committee (SteerCo) held December 14th, 2022.

The following topics were discussed:
- W3C news
- HAPJS open source Quick App engine
- Academia challenges
- HeritageIn open source Quick App project
- Community news


If you have any comments, thoughts, or suggestions please contact us on the mailing list (quickapp@ow2.org), or via [chat](https://rocketchat.ow2.org/group/QAI-Town-Square).

Happy holidays!


---

- [Support slides](/docs/SteerCos/20221214_qai_steerco/20221214_QAI_SteerCo_support.pdf "QAI SteerCo 2022-12-14 Slides")
- [Minutes](/docs/SteerCos/20221214_qai_steerco/20221214_QAI_SteerCo_MoM.pdf "QAI SteerCo 2022-12-14 Minutes")

---

The next SteerCo will be held **Wednesday, March 1st, <INS>2023</INS> at 10am CET**.

Many thanks for your support and motivation. See you all soon.
<br>The QAI team

