---
type: post
title: Steering Committee no.2
subtitle: September 22nd 2021
author: Christian Paterson
date: 2021-10-07
highlight: true               # true or false -> true will be shown on the homepage 
tags: ["steerco"]
bigimg: [{src: "/img/1308819_round-table.jpg", desc: "Round-table discussion"}]
---

Please find below links to the support slides and minutes from our 2nd QAI Steering Committee (SteerCo) held 22nd September 2021.

Although not all QAI participants could make the call, there was a lively discussion notably about:
- Starting outreach efforts into Russia;
- A [Sustainability study proposal](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/blob/master/task-forces/TF%20Sustainability/QAI_Task_Force__Sustainable_Fashion_Industry_Model_.pdf) about leveraging quick apps in the Apparel and Footwear industry;
- Feedback from the [Games Task Force](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/issues/15);
- Insight into our combined participation to the [Open Souurce Experience event in Paris](https://www.opensource-experience.com/en/);
- A quick "guide" on how QAI collaboration can be done.

We also enjoyed the participation of Gaël Duval from the [e Foundation](https://e.foundation/), and had a lively debate about what it means to be "open".

If you have any comments, thoughts, or suggestions please contact us on the mailing list (quickapp@ow2.org), or via [chat](https://rocketchat.ow2.org/group/QAI-Town-Square). You can also create an issue within [GitLab](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/boards).

---

- [Support slides](/docs/SteerCos/20210922_qai_steerco/20210922_QAI_SteerCo_support.pdf "QAI SteerCo 2021-09-22 Slides")
- [Minutes](/docs/SteerCos/20210922_qai_steerco/20210922_QAI_SteerCo_MoM.pdf "QAI SteerCo 2021-09-22 Minutes")

---

And don't forget, the **next SteerCo is on 8th December 10am CET**.

Many thanks for your support and motivation.
See you all soon for an action packed Q4!

<br>The QAI team
