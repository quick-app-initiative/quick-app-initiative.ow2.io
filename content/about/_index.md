---
title: About us
subtitle: About the MiniApp Initiative
section: About
date: 2021-11-29
revision: 2023-12-20
comments: false
---

The **[OW2 MiniApp Initiative](https://www.ow2.org/view/miniapp/)** explores technologies, use cases and business dynamics centered on *MiniApps*, a new paradigm of light, no-install, hybrid applications for smart devices.

> Who are the particpants? [Find out here](/page/participants/).

The initiative is a respectful, consensus driven community, founded on the ideals of *open source* (transparency, collaboration, sharing, open to all). It operates in accordance with a community agreed [charter](https://miniapp-initiative.ow2.io/docs/charter.pdf), and is overseen by [OW2](https://www.ow2.org/view/Main/), one of Europe's preeminent open source organisations.

- **Learn** about MiniApps as implemented by Quick Apps in the [White Paper](https://miniapp-initiative.ow2.io/page/whitepaper)
- **Subscribe** to the [mailing list](https://mail.ow2.org/wws/subscribe/miniapp?previous_action=info)
- **Get stuck in** with the [Quick App starter guide](https://miniapp-initiative.ow2.io/developers/guide/getting-started.html)
- **[Ask the team a question](mailto:miniapp-team@ow2.org?subject=I%20have%20a%20question%20about%20MAI)**

