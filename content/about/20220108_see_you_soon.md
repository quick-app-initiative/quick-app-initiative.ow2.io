---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: Sounds interesting
subtitle: Tell me more
section: About
date: 2022-01-08
bigimg: [{src: "/img/posts/2021/coding.png", desc: "Coding (Sora Shimazaki, Pexels)"}]
comments: false
---

Here's a list of key documents, tools and channels being used by the initiative.

<!--more-->

### Reference documents

MiniApps:
- [W3C MiniApp standards](https://www.w3.org/TR/mini-app-white-paper/#what-is-miniapp)

Quick Apps:
- [General presentation](/docs/OW2-Quick-App-Initiative-General-Presentation.pdf)
- [White Paper](https://miniapp-initiative.ow2.io/page/whitepaper)
- [Primer](https://miniapp-initiative.ow2.io/docs/Quick_App_Primer.pdf "Quick App Primer in PDF format")

OW2 & QAI policies:
- The [QAI charter](/docs/charter.pdf)
- The [OW2 bylaws and policies](https://www.ow2.org/view/Membership_Joining/Legal_Resources)
- The [OW2 Code of Conduct](https://www.ow2.org/view/Membership_Joining/Code_of_Conduct)


### Initiative resources

Collaboration tools:
- GitLab space: https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative
- Rocket.Chat discussion space: https://rocketchat.ow2.org/group/QAI-Town-Square

Information portal:
- General information: https://miniapp-initiative.ow2.io/
- Developer documentation: https://miniapp-initiative.ow2.io/developers/guide/

Social media:
- Twitter: [@OW2QuickApps](https://twitter.com/OW2QuickApps)
- Hashtag (for Twitter and LinkedIn): #QuickAppsEU

### See you soon

Everyone is welcome to participate to the initiative; [find out how here](/get-involved/).<br>
There are **no fees** to participate to the initiative.<br>
There are **no obligations** to [join OW2](https://www.ow2.org/view/Membership_Joining/On_Line_Registration) (although they appreciate it).

- To **join QAI**, register here: https://www.ow2.org/view/QuickApp/Participants_Form
- To **follow initiative news**, sign up to the mailing list: https://mail.ow2.org/wws/info/quickapp
- To **ask the team a question**, email us here: [quickapp-team@ow2.org](mailto:quickapp-team@ow2.org?subject=I%20have%20a%20question%20about%20QAI)

