---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: QAI Meetup Report
subtitle: July 1st, Paris
date: 2022-07-07
tags: [event,meetup]
author: Christian Paterson
bigimg: [{src: "/img/posts/2022/np_Shot_of_group_of_friends_taking_a_selfie_at_party_0K6ezb_free.jpg", desc: "Shot Of Group Of Friends Taking A Selfie At Party by Jacob Lund Photography from NounProject.com"}]
comments: true
draft: false
---

QAI had a great meetup in Paris on July 1st with Aspiegel, Huawei, OW2 and Vonage sharing lots of ideas. Read on for a short summary.

<!--more-->
## Aspiegel and Vonage love Quick Apps!

{{< rawhtml >}}
<img src="/img/posts/2022/vonage_love_qa.jfif" alt="Vonage love Quick Apps photo" align="right" height="auto" width="250">
{{< /rawhtml >}}

Both organisations are excited to work with the QAI community:<br><br>

*[Vonage](https://www.vonage.com/)* are looking to bring their VoIP functions to Quick Apps, add Quick App help documentation to the Vonage website, and work on pilot Quick Apps with the community. They also kindly offer QAI some much needed DevRel support (answering technical questions posted to the community, participation to hacks and meetups, ([social media postings](https://t.co/ZdwirzRjeo)), fireside chat videos, etc.). Vonage will also have a stand at [OSXP'22](https://www.opensource-experience.com/en/). How cool is that!<br><br>

*[Aspiegel](https://www.aspiegel.com/)* are also really motivated to help QAI become a big success. They are offering to help organise hack events, host QAI fireside chats and join in our social media and promotion efforts. They can also work with all you great developers to help publish your Quick Apps. Fantastic!

(As an aside, Aspiegel also mentioned the *Apps UP 2022 contest* - PR link below. It looks very cool ... AND there are **$$ prices to be won**. Always nice!)

## Conference pilot app idea

One of the great ideas for a pilot Quick App is an app to accompany conferences and trade shows.
  - Interactive exhibitor map (with indoor location using beacons and related app functions ... if we manage it)
  - Virtual tressure hunts
  - Who's the biggest walker leaderboards
  - Notifications for "saved" talks that are about to begin
  - ...

If the community can make such an app that adds nice user value, maybe we can showcase it at OSXP (to be seen). The question is how best to get the community involved. Especially in a timely manner (the app needs to be running early October).

One strong contender is a **live streamed 24hr hack event** at Huawei's [DIGIX Lab](https://developer.huawei.com/consumer/en/DIGIXLab-Offline/) in Dublin towards the end of September (thanks Aspiegel for that cool idea!).

[Sounds like fun, tell me more!](mailto:quickapp-team@ow2.org?subject=QAI%20code%20hack%20-%20Dubin,%20September)

## Social health fitness pilot app idea

Another great idea for a pilot Quick App was discussed during the meetup; an app focused on encouraging fitness activities -especially for the elderly and ageing. For example, walk (or other) challenges triggered when a person scans a QR code in their local park.

Something very friendly, simplified controls. Scan, start challenge, go. Maybe even a “friends group” chat function would also be cool. So bascially, 2 buttons on the screen. **Simple, accessible, playful, fun.**

[What do you think, want to help build this?](mailto:quickapp-team@ow2.org?subject=QAI%20elderly%20ageing%20fitness%20app%20idea)

## Cultural heritage pilot app extension

Mid July, Zach from Vonage and Martin from Huawei will work on bringing a "*click to call*" button to the [Cultural Heritage app](/news/2022-06-08-your-town-deserves-a-quick-app/).

[Coding, yummy and nutritious, let me help!](mailto:quickapp-team@ow2.org?subject=QAI%20cultural%20heritage%20app%20extension)


## Other points discussed

- The possibility to extend Quick App developer documentation with a section for 3rd party services.
  - Vonage call functions would be an obvious example.
- Discussion about the merit of proposing to the W3C a new <talk> HTML element, at least with respect MiniApp.

## And last but not least

What a wonderful meetup lunch we had! Yum!

**See you all next time.<br>
Until then stay well and enjoy the summer break.**

---
Support Materials

- Vonage: [presentation slides](https://devwithzachary.github.io/presentations/vonage-quickapps-meetup/deck.html)
- Aspiegel: [Apps UP contest July 2022 Press Release](/docs/Misc/Apps_UP_Returns_with_Over_USD1_Million_Prize_Money_July_4_2022.pdf)


