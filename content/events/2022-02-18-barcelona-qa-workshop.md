---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: Barcelona Quick App workshop
subtitle: Tuesday 1st March
date: 2022-02-18
tags: [workshop,barcelona,event,mwc]
author: Christian Paterson
bigimg: [{src: "/img/calendar.jpg", desc: "Calendar"}]
comments: true
draft: false
---

Who doesn't like a Quick App workshop?

Martin & Christian will be hosting an **in-person interactive discussion** about the future of mobile and Quick Apps in **Barcelona** on the morning of March 1st.

<!--more-->

| Time (CET)  |subject |
|-------------|--------|
| 9h30 | Welcome coffee |
| 10h00 | Christian Paterson - QAI community |
| 10h10 | Simon Phipps: Open source revolution in industry now coming to mobile |
| 10h30 | Christian Acosta: Education going mobile |
| 10h50 | Martin Alvarez-Espinar: MiniApp standards |
| 11h00 | Pause |
| 11h15 | Interactive discussion |
| 13h00 | End of workshop |

Join us at: AC HOTEL SOM C/ Arquitectura 1-3 08908 L'Hospitalet de Llobregat, Barcelona

Hope to see you there!
