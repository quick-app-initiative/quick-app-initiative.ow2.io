---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: OW2con'23
subtitle: Paris, June 14th - 15th
date: 2023-05-05
tags: [ow2,event,ow2con]
author: Christian Paterson
bigimg: [{src: "/img/calendar.jpg", desc: "Calendar"}]
comments: true
draft: false
---
Reserve your agendas, book your place, OW2con is back!

OW2con is one of the key events on the European open source calendar not to be missed. And a unique opportunity for the OW2 community to get together, catch up, exchange ideas, hear about new projects and technologies, and talk with open source industry movers and shakers. For several years the pandemic obliged a virtual event, but this year in-person is most definitely back with an event hosted by Orange at their Innovation Campus in Paris-Châtillon.

<!--more-->
***Open Source Software and Digital Commons***

Amazing expert speakers, fantastic breakout sessions, the OW2 technical council re-boot, and world-class keynote speakers:
- Philippe LATOMBE, Member of French Parliament
- Wolfgang GEHRING, FOSS Ambassador for Mercedes-Benz Tech Innovation
- Jean-Luc DOREL, Programme Officer, European Commission

Of course, OW2con is not OW2con without a little bit of MiniApp love. Both [Christian](https://www.ow2con.org/view/2023/Abstract_Community_Day#15060950) and [Martin](https://www.ow2con.org/view/2023/Abstract_Community_Day#15061500) will be giving talks on the Thursday about their experiences with QAI, MiniApps and Quick Apps.

**Check out the full OW2con'23 program [here](https://www.ow2con.org/view/2023/Program?year=2023&event=OW2con23)**

And the day before OW2con there is the **highly anticipated** MiniApp flavoured *web technologies for applications* event endorsed by the W3C, Systematic and OW2. Find out more [here](https://systematic-paris-region.org/evenement/web-technologies-for-applications-workshop-and-coding-contest/) and **spread the word!**

All in all, June 13 to June 15 is promising to be 3 days of pure MiniApp and open source excellence in lovely Paris. Make sure you reserve your free participation to these events soon; space is limited.

- **Register for [OW2con’23](https://nextcloud.ow2.org/index.php/apps/forms/LgLnmrdpQMybqrSD)**
- **Register for [Web Technologies for Applications](https://my.weezevent.com/web-technologies-for-applications-workshop-coding-contest)**

See you all soon in Paris!

{{< rawhtml >}}
<a href="https://www.ow2con.org:443/view/2023/"><img alt="OW2con′23 Annual conference:" src="https://www.ow2con.org:443/download/2023/Help_To_Promote/ow2con23_banner_468x60.jpg" title="OW2con′23 Annual conference :"/></a>
{{< /rawhtml >}}
