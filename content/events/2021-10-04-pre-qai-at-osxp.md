---
type: post
# Do not remove 'type: post'
highlight: true
# highlight = true to show the excerpt on the homepage 
title: QAI will be at Open Source Experience Paris
subtitle: Get ready for demos and talks 
date: 2021-11-05
tags: [osxp,event]
bigimg: [{src: "/img/posts/2021/conference.jpg", desc: "Photo by Terren Hurst on Unsplash"}]
comments: true
draft: false
---

The expectations are off the charts for the the [Open Source Experience](https://www.opensource-experience.com/en/) (OSXP) mega event in Paris next week. If you haven't already sorted your <INS>free</INS> [entry pass](https://www.sido-osxp.com/en/registration/inscription-opensource-experience), travel and hotel you need to hurry! **Only a few days to go**.

<!--more-->

Combined with [SIDO](https://www.sido-paris.com/en/), this dual event is going to be turbo-charged ... What a link-up; open source, IOT, robotics, AI:
- 12 000 visitors anticipated (respecting all sanitary measures of course).
- 200 speakers and 70 exhibitors (and that's just on the OSXP side of things)

**Boom!**

{{< rawhtml >}}
<IMG src="/img/posts/2021/728x90_OSXP_GIF.gif">
{{< /rawhtml >}}

**And, yes the OW2 Quick App Initiative will be there in force!**

Thanks to Huawei, who are OSXP gold sponsors, not only does QAI have access to a custom designed stand on the exhibitor floor, we also get to host a panel discussion with some very cool **industry heavyweights**. Plus, the custom design carries over to a full page advert in the visitor guide, as well as a hand-out flyer.

And, there's more!
<br>Mr Martin Alvarez Espinar (TEDx speaker supremo) has also had his talk accepted!

So let's list out the key QAI elements:
- Stand D24
  - kindly provided by Huawei.
  - Live quick app demos from [FRVR](https://frvr.com/) and [Famobi](https://famobi.com/?locale=en) (prepare to be seriously impressed).
  - Scripting demos with Martin and Zach (who? Zach, the QAI DevRel and [Twitch aficionado](https://www.twitch.tv/devwithzachary)).
- [Panel discussion](https://www.sido-osxp.com/en/session/9c5fd218-1e28-ec11-ae72-a04a5e7d345e) (Tuesday 9th @ 12h20 CET)
  - 45 minute + live streamed
  - Hosted by the one and only [Zachary Powell](https://www.linkedin.com/in/zachary-mg-powell/).
  - [Gaël Duval](https://www.linkedin.com/in/gaelduvalprofile/); the legend behind [/e/](https://e.foundation/).
  - [Kevin Bernatek](https://www.linkedin.com/in/kevinbernatek/); Business Development Manager at games company Famobi.
  - [Kester Bishop](https://www.linkedin.com/in/kesterbishop/); Strategic Project Lead at games company FRVR.
  - [Arjen Noorbergen](https://www.linkedin.com/in/arjen-noorbergen-2932b8/); founder of IOT Olisto; enabling smart things and digital services to work together.
- [Conference talk](https://www.sido-osxp.com/en/session/925fd218-1e28-ec11-ae72-a04a5e7d345e) (Wednesday 10th @ 13h50 CET)
  - 15 minutes + live streamed
  - Martin will be taking a look at "MiniApps: Quick Development, Superb UX"

Our hosting foundation (OW2) will also be organising a "find all the OW2 stands" game with a free raffle draw.
<br>... and if previous events are anything to go by, you won't want to miss out on their world famous beer party at the end of day 1.

### How can you join the fun?

Come to Paris of course!

But if that's not possible, the event has an [online platform](https://www.sido-osxp.com/en).
<br>(here's a [QAI mention](https://www.sido-osxp.com/en/offering/94fae52b-9b3c-ec11-981f-a04a5e7cdc9e)).

### Social media

We will be living tweeting from [Twitter](https://twitter.com/OW2QuickApps). Tweet. Tweet.
<br>**Please follow the account, like and re-share.**

You can also join the fun by using the hashtags _[#QuickAppsEU](https://twitter.com/search?q=%23QuickAppsEU)_ and _[#OSXP2021](https://twitter.com/search?q=%23OSXP2021)_.
<br>(and if you're a fully charter signed-up QAI participant, you're welcome to help us tweet from the account)

### What else?

You want more?! 😮

Did I mention that there will also be QAI talks at [SFScon](https://www.sfscon.it/) later in the same week?
- [Leveraging the open source dynamic to explore the MiniApp/Quick App dynamic](https://www.sfscon.it/talks/leveraging-the-open-source-dynamic-to-explore-the-miniapp-quick-app-dynamic/)
- [Quick App Initiative and beyond](https://www.sfscon.it/talks/quick-app-initiative-and-beyond/)

Oh yes, and don't miss the [new OpEd](https://miniapp-initiative.ow2.io/editorials/2021-11-04-frvr-blogpost-instant-future/) (opinion editorial). Thanks Claudio!

### Stop, it's all too much 🤯

OK

Ciao for now!
<br>Stay safe and maybe see you next week.

\- Big love -

<br>The QAI team

