---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: QAI Meetup!
subtitle: July 1st, Paris
date: 2022-06-07
tags: [event,meetup]
author: Christian Paterson
bigimg: [{src: "/img/posts/2022/np_Shot_of_group_of_friends_taking_a_selfie_at_party_0K6ezb_free.jpg", desc: "Shot Of Group Of Friends Taking A Selfie At Party by Jacob Lund Photography from NounProject.com"}]
comments: true
draft: false
---

We're really excited to confirm that QAI will hold a meetup in Paris this Friday, July 1st.
It's looking to be a great opportunity to speak with the community, listen to some presentations, explore ideas and coding.

<!--more-->

**LOCATION: Huawei Open Labs - 101 Bd Murat, 75016 Paris**

> **Provisional Agenda**
> - 09:30 – 10:00 Welcome coffee
> - 10:00 – 11:00 QAI participant presentations
> - 11:00 – 11:30 Quick App coding using the [open source cultural heritage app](/news/2022-06-08-your-town-deserves-a-quick-app/)
> - 11:30 – 12:00 University challenges
> - 12:00 – 13:30 Lunch and networking
> - 13:30 - 15:00 Quick App code lab and/or exploring collaboration ideas (depending on participants)
> - 15:00 – 15:30 Wrap up

Presentations from:
| Company | Company Domain | Speaker |
| ------ | ------ | ------ |
| *[Aspiegel](https://www.aspiegel.com/) (Ireland) | Innovative apps and services for Huawei Mobile Services in Europe and more| Fiona Boyle,  European brand manager |
| [Olisto](https://olisto.com/) (Holland) | Smart objects and service interconnectivity | Arjen Noorbergen, Founder |
| [Startin'blox](https://startinblox.com/en/) (France) | Web 3.0 and data decentralisation | Sylvain Le Bon, Co-Founder |
| *[Telanto](https://telanto.com/) (Spain) | Industry-University challenges | Christian Acosta-Flamma, Founder and CEO |
| [Vonage](https://www.vonage.com/) (USA) | VoIP and programmable communications APIs | Zachary Powell, Sr Android Developer Advocate |

**==> If you would like to join this meetup, please [let us know](mailto:quickapp-team@ow2.org?subject=QAI%20Meetup%201st%20July)**

See you Friday!

PS. Check out Zac and Christian's [TikTok teaser](https://t.co/ZdwirzRjeo) for the meetup.

(*) via remote
