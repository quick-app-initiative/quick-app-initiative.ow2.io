---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: OW2con'22
subtitle: Reserve your agendas for a big QAI session!
date: 2022-05-23
tags: [ow2,event,ow2con]
author: Christian Paterson
bigimg: [{src: "/img/calendar.jpg", desc: "Calendar"}]
comments: true
draft: false
---

The Quick App Initiative is pleased organise a special session focused on mobile and web technologies in the forthcoming [OW2con'22 event](https://www.ow2con.org/view/2022/).

Get insight into emerging web technologies, see how Quick Apps have driven business success in China, discuss with industry luminaries about the future of  mobile apps.

<!--more-->

This is going to be a [hot session](https://www.ow2con.org/view/2022/Abstract_Community_Day#08061615) - reserve your agendas now!

June 8th, 4.15pm **Talks**
1. *“General trends in mobile app technologies”* (Martin Alvarez – Huawei)
2. *“The real web3. Interoperability and decentralization”* (Benoit Alessandroni – Startin’Blox)
3. *“How Quick Apps Help Developers Achieve Business Growth in China”* (LiuZhao – Huawei China)
4. *“The OW2 Quick App Initiative”* (Christian Paterson – Quick App Initiative)
5. *“Coding a real Quick App”* (Martin Alvarez – Quick App Initiative)

June 8th, 4.55pm **Live Panel**
- Title: *“What will mobile look like in the future and how best to prepare”*
- Moderator: Christian Paterson
- Panellists:
    - Thomas Steiner (DevRel for Project Fugu, **Google**)
    - Gael Duval (founder & CEO, **e Foundation**)
    - Ilker Aydin (founder & CEO, **Famobi**)
    - Sylvain Le Bon (founder & CEO, **Startin’blox**)

Check out the full event program [here](https://www.ow2con.org/view/2022/Program?year=2022&event=OW2con22)

