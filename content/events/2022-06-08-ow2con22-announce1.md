---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: OW2con'22 coming up
subtitle: June 8-9
date: 2022-04-07
tags: [OW2con,event,online]
author: Christian Paterson
bigimg: [{src: "/img/posts/2022/ow2con22_Online2000x580L.jpg", desc: "OW2con'22"}]
comments: true
draft: true
---

It's that time of year again when the best, the brightest, the most wonderful people gather in celebration of OW2. Yes, that's right, it's OW2con time! And QAI will be there in style.

<!--more-->

OW2con is OW2's annual convention. It brings together people from far and wide to participate to some really very cool talks concerning open source, OW2 projects, technology trends and even discussions about such topics as sustainability and sovereignty.

To mark QAI’s first year anniversary (yes, you heard right – we’ll be 1 year old this June), we’ve been invited by the organisers to put together a special package for you. Here’s our plan:

> **Part 1 : talks**<br>
    > _**New trends in mobile app development**_
> - _General trends in mobile app technologies_ ([Martin Alvarez-Espinar](https://www.linkedin.com/in/espinr/) – Huawei)
> - _The real web3.0: Interoperability & decentralization_ ([Benoit Alessandroni](https://www.linkedin.com/in/benoitalessandroni/) – Startin’blox)
> - _How Quick Apps Help Developers Achieve Business Growth in China_ (Lionet Zhao – Huawei China)
> - _The OW2 Quick App Initiative_ ([Christian Paterson](https://www.linkedin.com/in/christian-paterson-35a210/) – Open Up/Huawei)
> 
> **Part 2 : live panel**<br>
    > _**What will mobile look like in the future and how best to prepare**_
> - Chairperson: Christian Paterson
> - [Thomas Steiner](https://blog.tomayac.com/) (DevRel, Google - focused on the Web and [Project Fugu](https://www.chromium.org/teams/web-capabilities-fugu/))
> - [Gaël Duval](https://www.linkedin.com/in/gaelduvalprofile/) (founder, [/e/](https://e.foundation/) and Murena)
> - [Ilker Aydin](https://www.linkedin.com/in/aydinilker/) (founder & CEO, [Famobi](https://famobi.com/?locale=en))
> - plus, a surprise panellist

**Sign-up now & reserve you agendas!**

PS. The QAI session starts at 16h15 on June 8th (click the image to find out more).

{{< rawhtml >}}
<a href="https://www.ow2con.org/view/2022/"><img alt="OW2con′22 Annual conference:" src="https://www.ow2con.org/download/2022/Help_To_Promote/ow2con22_online_Logo_160x100.jpg" title="OW2con′22 Annual conference :"/></a>
{{< /rawhtml >}}
