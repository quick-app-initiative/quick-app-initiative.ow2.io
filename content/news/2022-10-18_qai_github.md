---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: QAI on GitHub
subtitle: 1st project is Heritage In
author: Christian Paterson
date: 2022-10-18
tags: ["github","code","opensource","demoapp"]
bigimg: [{src: "/img/mockups.jpg", desc: "MiniApps"}]
highlight: true
draft: false
comments: true
---

Yes it's true, QAI now has a **collaboration space on GitHub** woohoo. This compliments the space we also have on OW2's [GitLab](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative) service. Now _you_ choose where to collaborate with the community.

<!--more-->

Find the new QAI GitHub space here : https://github.com/ow2-quick-app-initiative

# The _Heritage In_ project

Within this new GitHub space you can now find the [Heritage In](https://github.com/ow2-quick-app-initiative/poi-quick-app) project; a great _showcase_ of what can be done with **open source** and **open data** ... and what's more, the project produces Quick App <ins>and</ins> PWA versions of your implementation. So no worries if your audience don't have Quick App supporting smartphones.

There are several [demo implementations](https://github.com/ow2-quick-app-initiative/poi-quick-app-implementations/tree/main/quick-app) to give you some ideas, so why not download the project today and create _your own_ Heritage In app.

And, of course, we would love to work with you to expand this project.
- What new functions would you like to see?
- What new functions could you add?
- What amazing implementations can you create?

**We're excited to see _your_ ideas ... let the collaboration begin!**
