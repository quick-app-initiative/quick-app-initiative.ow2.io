---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: New Presentation Available
subtitle: Sharing is caring
date: 2022-02-18
tags: [presentation]
author: Christian Paterson
bigimg: [{src: "/img/mockups.jpg", desc: "Mockups"}]
comments: true
draft: false
---

We have updated our general presentation about Quick Apps and the initiative.<br> Now, it includes a number of example use cases to spark your imagination and creative juices.

<!--more-->

The presentation can be found in the "About" section, or via this [direct link](/docs/OW2-Quick-App-Initiative-General-Presentation.pdf).<br> **Share widely!**

And as always, if you would like us to present the initiative to your team or community, please contact us: [quickapp-team@ow2.org](mailto:quickapp-team@ow2.org?subject=Can%20you%20tell%20me%20more%20about%20Quick%20QApps).


