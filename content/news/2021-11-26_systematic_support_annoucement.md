---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: Systematic support
subtitle: Hub Open Source announce their support for QAI
date: 2021-11-26
tags: [recognition,community,systematic]
author: Christian Paterson
bigimg: [{src: "/img/posts/2021/husna-miskandar-FC4z3l4sUYc-unsplash.jpg", desc: "Group of happy people jumping (Photo by Husna Miskandar on Unsplash)"}]
comments: true
draft: false
---

In November 2021, the initiative proudly gained peer recognition from [Systematic's Open Source Hub](https://systematic-paris-region.org/hubs-stakes/open-source-hub/?lang=en); a deep-tech innovation pole for the Paris region.

<!--more--> 

![Systematic Logo](/img/participant-logos/systematic.png)

You can read their support announcement (in French) [here](https://systematic-paris-region.org/le-hub-open-source-apporte-son-soutien-au-projet-de-lassociation-europeenne-ow2-sur-les-quick-apps/).

Many thanks Systematic!<br>
The initiative is looking forward to working with you in 2022 and beyond.

