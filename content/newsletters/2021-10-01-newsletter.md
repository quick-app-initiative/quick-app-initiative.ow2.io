---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: false
title: Quick App Initiative Newsletter 
subtitle: October 2021
date: 2021-10-08
tags: ["newsletter"]
bigimg: [{src: "/img/bulletin.jpg", desc: "Bulletin board"}]
comments: false
---

Summer is ending, autumn is arriving, and QAI is going from strength to strength!

Highlights this month include:
- [Opinions matter](#have-your-say)
- [Now you can register](#join-qai)
- [SteerCo](#qai-steerCo)
- [Task Force News](#task-force-news)
- [Pilots and POCs](#pilots-and-pocs)
- [Other news](#other-news)
- [Promotion](#promotion)
- [Housekeeping](#housekeeping)

<!--more-->

### Have your say

The QAI portal has been revamped to provide a better distinction between content types. It now has a space for your _thoughts_, _feedbacks_, _dreams_, _rants and raves_ about quick apps and the initiative. We've started the ball rolling by looking at 2 questions:
1. [10 reasons for quick apps](https://miniapp-initiative.ow2.io/editorials/2021-09-10-quickapps-to-the-rescue/)
2. [Moving beyond addressable market](https://miniapp-initiative.ow2.io/editorials/2021-09-10-building-european-ecosystem/)

You too can submit editorials for this section 👍

If writing an article interests you, drop us a line at [quickapp-team@ow2.org](mailto:quickapp-team@ow2.org?subject=My%20opinion%20piece%20idea).


### Join QAI

The QAI registration process up and running. "Hurrah" I hear you say 😄

**[Sign-up](https://www.ow2.org/view/QuickApp/Participants_Form) now** to be part of the fantastic QAI community. Just fill in the super simple form, agree to the charter, and submit. Job done.

Why would you want to do this:
- Be part of creating a new app ecosystem;
- Get your name on the [participant's page](https://www.ow2.org/view/QuickApp/Participants) (pretty cool right)?
- Get full access into our OW2 collaboration tools ([GitLab](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative) and [Rocket.Chat](https://rocketchat.ow2.org/group/QAI-Town-Square));
- Get SteerCo invites;
- Experience that warm and fuzzy feeling (and much kudos) of knowing your support is appreciated.


### QAI SteerCo

The initiative's "back to school" steering committee has just been held. As always when talking with QAI people, lots of energy, lots of ideas. Read all about it [here](https://miniapp-initiative.ow2.io/meetings/2021-09-22-steerco/).


### Task force news
Just a quick highlight in this newsletter as more details can be found in the steering committee post mentionned above.

- Gaming: **FRVR** and **Famobi**  have been looking at quick apps in gaming. The task force has started to outline a roadmap for advancing the subject. Why not drop Kester, Claudio, Kevin, Chris or Ilker a DM on Rocket.Chat if you're interested in this work.

- Sustainability: **Alliance Tech** have proposed a study to investigate how quick apps could help improve sustainability within the _apparell and footwear industry_ all the while enabling new customer experiences, supporting SMEs, bringing different end-to-end actors in this fast moving and important industry closer together. Read the proposal [here](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/blob/master/task-forces/TF%20Sustainability/QAI_Task_Force__Sustainable_Fashion_Industry_Model_.pdf). If this sounds interested, please reach out to Konstantine through Rocket.Chat.

- Education: The education task force is currently on hold, but we hope to restart things before the end of the year. If using quick apps to empower students, teachers and researchers sounds interesting to you, why not [drop us a line](mailto:quickapp-team@ow2.org?subject=Tell%20me%20more%20about%20the%20Education%20TF).

### Pilots and POCs

[Lutece](https://lutece.paris.fr/lutece/) is the hugely successful open source citizen services platform managed by the City of Paris, and published through OW2.
- It is used not just by City of Paris, but also other cities like Lyon, Baltimore, Budapest, and some places in Africa.
- The Lutece team have really seen the potential for quick apps, and have agreed to work with the initiative to create a pilot app (or 2).
- If you're interested in being part of the development effort please [contact us](mailto:quickapp-team@ow2.org?subject=Tell%20me%20more%20about%20the%20Lutece%20QuickApp%20pilots).


### Other News

- Fantastic news; the initiative is now supported by Systematic's [Hub Open Source](https://systematic-paris-region.org/hubs-enjeux/hub-open-source/) 👏
  We're very much looking forward to working with the hub on bringing quick app awareness to all the amazing companies and organisations that make up Systematic's network in the Paris Region and beyond. "Watch this space" as they say.

- The initiative welcomes [Open Track Ltd.](https://opentrack.run/) from the UK. They're a very exciting company working in the athletics domain. We're all very excited to see how quick apps can improve the experience and immersion for sports spectators, competitors and officials.


### Promotion

Quick apps and the initiative will be showcased in various events this autumn.

| Date | Event |
| ------ | ------ |
|20 Oct	- 22 Oct | [DroidCon Berlin](https://www.berlin.droidcon.com/) |
|28 Oct	- 28 Oct | [Conf42: JavaScript 2021](https://www.conf42.com/JavaScript_2021_Martin_AlvarezEspinar_Quick_Apps_frictionless_UX) |
|09 Nov	- 10 Nov | [**Open Source Experience Paris**](/events/2021-10-04-pre-qai-at-osxp/) (Talk + Workshop + Stand) <br>This is the biggy!<br>- Try  quick app demos from Famobi and FRVR on our stand,<br>- Join the workshop with [e Foundation](https://e.foundation/), [Famobi](https://famobi.com//?locale=en), [FRVR](https://frvr.com/), [Huawei](https://www.huawei.com/en/) and [Olisto](https://brands.olisto.com/)|
|12 Nov	- 13 Nov | SFS Con ([Talk 1](https://www.sfscon.it/talks/quick-app-initiative-and-beyond/), [talk 2](https://www.sfscon.it/talks/leveraging-the-open-source-dynamic-to-explore-the-miniapp-quick-app-dynamic/))|

Feel free to join these events, say "hello", and show your support with some social network promotion ([#QuickAppsEU](https://twitter.com/search?q=%23QuickAppsEU&src=typed_query&f=live))

Are _you_ holding or participating to events that you think would be of interest to other QAI participants? Or maybe you think these events might be good exposure for quick apps and the initiative. [Let us know](mailto:quickapp-team@ow2.org?subject=Events) - sharing is caring.

 
### Housekeeping

Still need to create your OW2 user account and then connect to GitLab and Rocket.Chat? No worries, here are the relevant links:

- [OW2 user account](https://www.ow2.org/view/services/registration)
- [GitLab](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative)
- [Rocket.Chat](https://rocketchat.ow2.org/group/QAI-Town-Square)


That's all for now. Stay safe, stay appy, and see you soon! 
