---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: Quick App Initiative Newsletter 
subtitle: Summer 2022
author: Christian Paterson
date: 2022-07-28
tags: ["newsletter"]
bigimg: [{src: "/img/bulletin.jpg", desc: "Bulletin board"}]
comments: false
draft: false
---

Hip hip hooray, summer is here! School's out, the sun is shining (a bit too much in many places), bees are buzzing, ice cream is melting, cool drinks are flowing, and QAI is advancing. Highlights for this newsletter:
- [May-June events](#may-june-events)
- [Quick Apps for cultural heritage](#quick-apps-for-cultural-heritage)
- [Vonage loves Quick Apps](#vonage-loves-quick-apps)
- [Fireside chats](#fireside-chats)
- [Linux Foundation Open Source Summit Europe](#linux-foundation-open-source-summit-europe)
- **[Quick App hack event](#quick-app-hack-event)**
- **[Open Source Experience Paris](#open-source-experience-paris)**
- [Industry-university education challenges](#industry-university-education-challenges)
- **[Open source Quick App engine](#open-source-quick-app-engine)**

<!--more-->

### May-June events
QAI was nicely represented at several spring events: EIT Digital’s annual conference (Brussels), [OW2con online](https://www.youtube.com/playlist?list=PL9hl5h3SGJkwfv4lqkOX-WBetjj6zOHdn), [Systematic Hub Day Open Source](https://systematic-paris-region.org/evenement/hub-day-open-source-20222/) (Paris), Cloud Datacenter + Infra (Paris).

--> Even if you’re short for time, check out this inspiring video about [Quick Apps in China](https://www.youtube.com/watch?v=5CFkU5qqte8&list=PL9hl5h3SGJkwfv4lqkOX-WBetjj6zOHdn&index=3).


### Quick Apps for cultural heritage
(working title)
An open source, easy to configure and repurpose Quick App “template” that displays points of interest on a map along with rich media information for each POI. The information (POI location, text, images, etc.) is drawn from open data sources and/or crowdsourced. Two proof of concept demos have been developed to highlight the ease and adaptability of the approach: Brussels Comic Book Route and Leuven Monuments. What could you and your town or city do?

--> Read more [here](https://miniapp-initiative.ow2.io/news/2022-06-08-your-town-deserves-a-quick-app/).


### Vonage loves Quick Apps
(well of course they do, who wouldn’t!)
Read all about it in the [QAI Paris meetup report](/events/2022-07-01_meetup_paris_report/).


### Fireside chats
Can start again thanks to [Aspiegel](https://www.aspiegel.com/)’s offer to host the chat videos (which QAI will then link to and promote of course).
Let me know if you would like to help organize chats, would like to be in a chat, know someone who should be in a chat.


### Linux Foundation Open Source Summit Europe
Dublin, September 13-16

If all goes according to plan, Quick Apps and QAI will be represented. Are you planning to be at the event?


### Quick App hack event
Dublin, late September/early October ... TO BE CONFIRMED.

The timing is very short, but we haven’t yet given up hope on this event. The aim is to create a Quick App for conference and tradeshow visitors. A 24hr social media shared challenge with the app being showcased at OSXP. We need your help if this is to be a success.

--> [Contact us](mailto:quickapp-team@ow2.org?subject=QAI%20Possible%20Dublin%20hack%20event) ASAP to be involved.


### Open Source Experience Paris
Paris, November 8-9

A nice chuck of the Huawei stand will be donated to QAI. Let's make this space together a fantastic interactive experience; demo your Quick Apps, help animate the QAI space, be a social media guru.

--> [Contact us](mailto:quickapp-team@ow2.org?subject=QAI%20at%20OSXP2022) ASAP to be involved.


### Industry-university education challenges
(In partnership with [Telanto](https://telanto.com/))

Time to get organised for the Autumn semester. If you’re a [registered](https://www.ow2.org/view/QuickApp/Participants_Form) QAI participant and have an idea for a code challenge, a study, a hack event that involves Quick Apps come speak to me and Martin. If the challenge is interesting, we’ll run it together, and of course publish the output as open source/creative commons on the QAI portal.


### Open source Quick App engine
You asked, so we dug, we dove, we ran, we climbed, we trekked jungles, we climbed mountains, we braved arctic tundra, we explored dank caves, we spoke in many tongues, we made offerings, we danced with tribes, we ate strange foods, we crossed borders, we scoured the internet, and we traversed space and time. In short, we hunted high and low … and yes, we found it – that’s right, an open source (Apache 2.0) release of the core Quick App engine! The code has been released as an [incubation project](https://openatom.cn/ProjectDetails/55b7b930dce811ec95fa7b6a915924b7) through the [OpenAtom Foundation](https://www.openatom.org/), and can be found in GitHub ([https://github.com/hapjs-platform/hapjs](https://github.com/hapjs-platform/hapjs)).
- Have we looked at it? Not yet.
- Have we delved its exotic depths? Nope.
- Is it usable? Your guess is as good as mine.
- Does it install on my Android phone ok? To be tested.
- Does it actually run a Quick App correctly? The million dollar question.
- Do you want to join a journey of discover? Yes! (I hope)

--> [Contact us](mailto:quickapp-team@ow2.org?subject=QAI%20opensource%20QA%20engine%20deep%20dive) and let’s start a QAI investigation Task Force!

### That's all folks

Feel free to chat on the QAI [RocketChat channel](https://rocketchat.ow2.org/group/QAI-Town-Square), post/share/like [QAI tweets](https://twitter.com/OW2QuickApps), read the latest [editorials](/editorials/), co-opt other companies to join QAI (sharing is caring remember), submit an article for the portal, invite the QAI community to events you’re organising or going to.

... above all, have a great summer!
