---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: 10 reasons for quick apps
subtitle: Changing the app game in Europe 
date: 2021-10-01
tags: ["OpEd"]
author: Christian Paterson
bigimg: [{src: "/img/posts/2021/jared-berg-k5Q1tVScDiU-unsplash.jpg", desc: "Life ring photo by Jared Berg on Unsplash"}]
draft: true
comments: true
---

Society is experiencing a remarkable period of digitalization yet consumers are tired of app hunting, developers are frustrated by app publication experiences, small businesses, local governments and NGOs often can't even afford an app presence, app marketplaces are jealously guarded, devices are awash with unused apps ... and still everything, everwhere is being pushed online. Quick apps offer a 10 point life ring in these rising tides.


<!--more-->
### Overchoice

Exploration, innovation, resilience and adaptation is inexorably shifting us online, yet also generating multiple pressures:
- Companies and public services increasingly need an app presence to reach their audiences, yet writing, publishing and updating apps is not very realistic or easy for many;
- App marketplaces are filled by millions of new apps each year, making it paradoxically harder for users to find apps, and frustratingly harder for businesses to get their apps seen;
- Sustainability goals beseech better resource efficiency, yet we download apps with abandon, consuming network bandwidth and bloating our devices;
- Study after study highlight the growing problem of _user app fatigue_[1][2], especially for _ad hoc_ needs.

> "Water, water, every where, nor any drop to drink"

Quick apps offer a future facing alternative to this stale dynamic by providing a breath of fresh air to all parties, and at the same time rethinking the way in which mobile apps can better support (in all senses) ubiquitous connectivity.

### Win win win

**For users**, quick apps enable **in-the-moment** experiences that forego the burden of _search and download_ for an _immediacy of utility_ (win 1). They deliver rich and powerful experiences, yet launch near instantly after scanning QR codes, clicking web links, or being selected from within other apps. They allow for new experiences (win 2), new ease of life services (win 3), less download hassles (win 4), less device bloat (win 5).

**For developers**, quick apps are orders or magnitude easier and faster to develop than equivalent native apps (win 6), and can also be published outside (as well as inside) traditional app marketplaces. Indeed, their publication/distribution routes are significantly increased (win 7). Quick apps run on a _quick app engine_ that sits above the device's operating system. The quick app engine not only takes care of the heavy lifting for app performance (hence the reduced complexicty and volume of code write), it also presents a normalised API set that abstracts from the OS and hardware. As the standards and device support for quick apps mature, these principles of abstraction and normalisation will empower a _write once, run anywhere_ dynamic (win 8).

**For businesses**, quick apps enable new oppoprtunities for audience engagement and opportunistic interactivity (win 9), driving concomitant market capture and brand awareness. What's more, since quick apps are both _easier_ and _faster_ to developer than traditional apps, skills are easier to find and projects require less effort. That's to say, having a quick app becomes more accessible to more businesses (win 10).

In short, quick apps facilitate the **democratisation of mobile apps**, increase **marketplace choice**, and provide a better **situational experience**. They also happen to consume fewer device resources.

### Nurturing a quick app ecosystem in Europe

Whilst quick apps implement _emerging_ **W3C standards**, the European ecosystem is still nascent; low business and developer awareness, insufficient tooling, limited device compatibility. To encourage growth and opportunity capture, the **Quick App Initiative** has been launched within **OW2**, a respected **European _open source_ organization**.

We believe that **diversity and open collaboration engender progress**, and that a strong European quick app ecosystem will encourage accessible innovation, increase consumer choice, reduce gatekeeper control, stimulate business opportunities, open channels to foreign martkets.

---

Please visit the **[web portal](https://miniapp-initiative.ow2.io/)**, or sign-up to the **[mailing list](https://mail.ow2.org/wws/subscribe/quickapp?previous_action=info)**

---

    [1] https://clevertap.com/blog/app-fatigue/
    [2] https://techcrunch.com/2016/02/03/app-fatigue/
