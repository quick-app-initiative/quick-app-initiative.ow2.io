# Website Based on Pages Template

This repository hosts the [OW2 MiniApp Initiative public website](https://miniapp-initiative.ow2.io/) using [Hugo](https://gohugo.io/), [GitLab Pages](https://pages.gitlab.io), and a template cased on [Beautiful Jekyll](http://deanattali.com/beautiful-jekyll/) theme.

**Table of Contents**  

- [Structure of the Website](#structure-of-the-website)
- [Create New Articles](#create-new-articles)
  - [Filename and URL](#filename-and-url)
  - [Write Articles](#write-articles)
- [Do you need raw HTML?](#do-you-need-raw-html)
- [Acknowledgements](#acknowledgements)

## Structure of the Website

The website structure includes two types of documents: 

- `pages`, to host content organized in the specific sections (about, documents, etc.).
- _articles_, with articles to be published under three different categories:
  - `news`, with objective pieces of news, like announcements.
  - `events`, with references to external and internal events.
  - `editorials`: opinion articles with different topics and orientations.
  - `newsletters`: periodic newsletters.


## Create New Articles

This website includes different types of articles: 

- **News**. Objective pieces of news.
- **Events**. Articles to announce conferences, meetups, and other kind of events (external and organized by the MiniApp Initiative).
- **Editorials**. Blog posts with subjective content. These articles could be technical or business oriented.   
- **Newsletters**. Periodic newsletters.   

### Filename and URL

New articles must be written in Markdown format. Files will be stored under the directory, depending on the type of the article:

- `/content/news`
- `/content/events`
- `/content/editorials` (`/content/opinions` is preserved for legacy and backwards compatibility, but do not use it)
- `/content/newsletters` 

Documents will follow the following filename template:

```
/content/{type-of-article}/{YYYY}-{MM}-{DD}-{hyphenized-lowercase-title}.md
```

Where:

- `{YYYY}` is the year of publication
- `{MM}` is the month of publication
- `{DD}` is the day of publication

The name should be consistent with the title of the post, and it will be used in the public URL. 

### Write Articles

Please, use the following metadata template (YAML data on top of the document):

```
---
type: post             # Don't worry, it indicates the layout
excerpt_separator: <!--more-->   
                       # Use <!--more--> to create a summary  
highlight: true        # true if you want this item on the homepage 
draft: true             # true to avoid listing the article
title: My Title        # Main title of the article (this will be the H1) 
subtitle: My Subtitle  # Subtitle
date: 2021-09-30       # Date in ISO format
tags: ["steerco"]      # List of tags if we need to organize articles
author: Name Surname   # Only for opinion items (remove the param otherwise) 
bigimg: [{src: "/img/mockups.jpg", desc: "MiniApps"}]
    # ^ Decorative image, leave this by default if you don't want to customize it
---

Some introductory text about the article. 
(Summary that will be shown in the articles list on the homepage)

(Use a common template for events):

When: Date  
Where: Location (and URL)

<!--more-->

More additional text...

```

## Do you need raw HTML?

There is a _shortcode_ called `rawhtml` you can use to include any HTML within the pages and articles.

Just wrap your HTML within these markers:

```
{{< rawhtml >}}
  <!--  your code here -->
{{< /rawhtml >}}
```


## Acknowledgements

A big thanks for the pictures to: 
- <a href="https://unsplash.com/@ronens?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Ronen Sigan</a> on <a href="https://unsplash.com/s/photos/bulletin?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
- <a href="https://unsplash.com/@halacious?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Halacious</a> on <a href="https://unsplash.com/collections/4680807/wireframes?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
- <a href="https://unsplash.com/@kyrie3?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Kyrie kim</a> on <a href="https://unsplash.com/s/photos/calendar?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
- <a href="https://unsplash.com/@pankajpatel?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Pankaj Patel</a> on <a href="https://unsplash.com/s/photos/code?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
        
