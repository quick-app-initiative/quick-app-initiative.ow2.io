## Steering Committee meetings


### naming conventions

- Directory names = yyyymmdd_qai_steerco <-- for example "20210708_qai_steerco"
- Support slides = yyyymmdd_QAI_SteerCo_support.pdf
- Minutes of meeting = yyyymmdd_QAI_SteerCo_MoM.pdf


### how to make the PDFs of minutes
1. Using [StackEdit](https://stackedit.io/app#), write the minutes in _markdown_.
2. Still using StackEdit, ctrl-p to print the contents (StackEdit chooses the rendered contents rather than the raw markdown - win!).
3. Select, "save to pdf" and save the output file to somewhere on your computer.
4. Upload the saved file GitLab.
