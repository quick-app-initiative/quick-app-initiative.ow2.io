This directory contains the JS files that enable the operation with the menus of the developer documentation. They are generated automatically via CI after every build of the main QAI repository. 

No need to preserve them since they will be regenerated every time a change is produced in the main repo.
